import ROOT
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
import scipy.interpolate as ip
from matplotlib.ticker import MultipleLocator

#-----------------------------------------------------------------------------#
def gauss(x, *p):
    '''Define gaussian distribution for fitting'''
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))
#-----------------------------------------------------------------------------#
def draw_random_samples_2D(x,y,n_samples):
    '''Draw a subset of random n_samples from a 2D array of points'''
    rnd_index=np.random.choice(np.arange(0,len(x)),int(n_samples))
    x=x[rnd_index]
    y=y[rnd_index]
    return x,y
#-----------------------------------------------------------------------------#
def draw_random_samples_Poisson(x,y,mu,flag_mean_case=False):
    '''
    Draw a subset of random samples from a 2D array of points.
    Use the Poisson mean mu to determine the number of sampled points
    '''
    if(flag_mean_case==True):
        n_samples = int(mu)
    else:
        n_samples = np.random.poisson(mu)
    rnd_index=np.random.choice(np.arange(0,len(x)),int(n_samples))
    x=x[rnd_index]
    y=y[rnd_index]
    return x,y
#-----------------------------------------------------------------------------#
def get_energy_isocontour_lindhard(x=1.,keVnr=1.0,k_Lindhard=1.,W=0.0137224,g1=0.118735,g2=83.44):
    '''Get an isocontour line in S1-S2 space.
       Return corresponding S2 values given a linear array of S1 values.'''

    y = g2*(keVnr*k_Lindhard/W - x/g1)
    return y
#-----------------------------------------------------------------------------#
def get_energy_isocontour(S1=1.,keVnr=1.0,g1=0.118735,g2=83.44,a=12.6,b=1.05):
    '''Get an isocontour line in S1-S2 space (assuming NEST v2 formula)
       Return corresponding S2 values given a linear array of S1 values.
   '''
    S2 = g2*( a*np.power(keVnr,b) - S1/g1 )
    return S2
#-----------------------------------------------------------------------------#
def get_counts_before_isocontour_energy_line(x,y,Emin_cut=1.5,Emax_cut=6.5,
    W=0.0137224,g1=0.118735,g2=83.44):
    '''Get number of counts before an isocontour energy line'''
    data = zip(x,y)
    counts = 0
    for index, val in enumerate(data):
        E_rec = W*( data[index][0]/g1 + data[index][1]/g2)
        if ((E_rec <= Emax_cut) & (E_rec >= Emin_cut)):
            counts += 1
    return counts
#-----------------------------------------------------------------------------#
def get_counts_before_line(x,y,f_line,verbose=False):
    '''Get number of counts before a line in X-Y space'''
    data = zip(x,y)
    counts = 0
    for index, val in enumerate(data):
        condition = val[1] < f_line(val[0]) and val[0] > 0.
        if (condition):
            counts += 1
            if(verbose):
                print ('X = {:.2f}, Y = {:.2f}, f_eval = {}'.format(val[0],val[1],f_line(val[0])))
    return counts
#-----------------------------------------------------------------------------#
def scatter_color(x,y,alpha=1,size=4,cmap=None,nsamples=1e4,zorder=1):
    '''Define color Scatter Plot'''
    if(len(x)>nsamples):
        rnd_index=np.random.choice(np.arange(0,len(x)),int(nsamples))
        x=x[rnd_index]
        y=y[rnd_index]
    xy=np.vstack([x,y])
    xy[np.isnan(xy)]=0
    xy[np.isinf(xy)]=0
    z = st.gaussian_kde(xy)(xy)
    plt.scatter(x,y,c=z,s=size,alpha=alpha,marker='o',cmap=cmap,zorder=zorder)
    return
#-----------------------------------------------------------------------------#
def find_percentile_line(x, y, w=0.5, s=0.1, P=50, xlim=-1, verbose=False, x_idx=0):
    '''Find the percentile line of a scatter plot
        - w: window where to take the mean
        - s: step size in the x-coordinate
        - P: percentile to compute, [0,100]
        - xlim: special range for x-variable, e.g. xlim=[0,100]
        Notes:
        * empty x-slices won't be allowed, so make the window large enough
    '''
    if isinstance(xlim,list):
        xmin=float(xlim[0])
        xmax=float(xlim[1])
    else:
        xmin=float(min(x))
        xmax=float(max(x))
    x_range = np.arange(xmin,xmax, s)
    q_y = np.empty_like(x_range)
    y_vals = np.empty_like(x_range)
    for index, val in enumerate(x_range):
        cut = (x < (val+w)) & (x > (val-w)) 
        q_y[index] = np.percentile(y[cut],P)
        if(val==x_range[x_idx]):
            y_vals = y[cut]
    if(verbose):
        return x_range, q_y, y_vals
    else:
        return x_range, q_y
#-----------------------------------------------------------------------------#
def find_mean_line(x, y, w=0.5, s=1, xlim=-1):
    '''Find the mean line of a scatter plot
        - w: window where to take the mean
        - s: step size in the x-coordinate
    '''
    if isinstance(xlim,list):
        xmin=float(xlim[0])
        xmax=float(xlim[1])
    else:
        xmin=float(min(x))
        xmax=float(max(x))
    x_range = np.arange(xmin, xmax, s)
    mean_y = np.empty_like(x_range)
    std_y = np.empty_like(x_range)
    for index, val in enumerate(x_range):
        cut = (x < (val+w)) & (x > (val-w)) 
        mean_y[index] = np.mean(y[cut])
        std_y[index] = np.std(y[cut])
    return x_range, mean_y, std_y
#-----------------------------------------------------------------------------#
def smooth_data(x, y, s=0.001):
    '''Create a smooth spline from 1D data'''
    #UnivariateSpline does not deal well with nan, so set the weight to 0 for all nan's
    w = np.isnan(y)
    y[w] = 0.
    f_spline = ip.UnivariateSpline(x, y, w=~w, s=s) #Tilde(~) is the invert operator
    return f_spline
#-----------------------------------------------------------------------------#
def pdf_contour(x,y,nsamples=1e4,nsig=2,color='b',fill=True,fill_alpha=0.2,\
                colormap=plt.cm.Reds, bins=50,label=True, lw=2, line_alpha=1,\
                zorder=1):
    '''Return contour levels'''
    if(len(x)>nsamples):
        rnd_index=np.random.choice(np.arange(0,len(x)),int(nsamples))
        x=x[rnd_index]
        y=y[rnd_index]
    xmin = min(x)
    xmax = max(x)
    ymin = min(y)
    ymax = max(y)
    x_range = np.linspace(xmin,xmax,bins)
    y_range = np.linspace(ymin,ymax,bins)
    pdf_kde = st.kde.gaussian_kde([x,y])
    
    # sample the pdf and find the value at different percentiles
    # nsamples=5000
    nsamples = int(nsamples)
    #to have a contour that contains X% of probability, I need the
    #(1-X)-th percentile
    sig1=np.percentile(pdf_kde(pdf_kde.resample(nsamples)), 100-68.27)
    sig2=np.percentile(pdf_kde(pdf_kde.resample(nsamples)), 100-95.44)
    sig3=np.percentile(pdf_kde(pdf_kde.resample(nsamples)), 100-99.73)
    q50=np.percentile(pdf_kde(pdf_kde.resample(nsamples)), 50.)
    q0=np.percentile(pdf_kde(pdf_kde.resample(nsamples)), 100-0.)
    # print sig1, sig2, sig3
    # print q0, q50

    levels=[sig1,sig2,sig3][:nsig]
    levels.reverse()#levels must be in increasing order
    linestyles=['-','-',':'][:nsig]
    linestyles.reverse()
    
    # Plot contour lines
    xi,yi = np.meshgrid(x_range, y_range)
    Z = pdf_kde([xi.flatten(),yi.flatten()])
    Z = Z.reshape(len(y_range),len(x_range))
    CS=plt.contour(x_range, y_range, Z, levels, linewidths=lw, alpha=line_alpha,\
                   colors=color, linestyles=linestyles, zorder=zorder)
    
    if fill==True:
        levels.append(q0) #Filled contours require at least 2 levels
        # levels.reverse() #bring 10 to the beginning
        plt.contourf(x_range, y_range, Z, levels, alpha=fill_alpha,cmap=colormap,\
                     zorder=zorder)
    if label==True:
        CS.levels = ['1$\sigma$','2$\sigma$','3$\sigma$'][:nsig]
        CS.levels.reverse()#levels must be in increasing order
        plt.clabel(CS, CS.levels, inline=True, fontsize=20)

    return
#-----------------------------------------------------------------------------#
def percentile_1D(v, q):
    '''Find the approximate percentile q of a 1D vector v.
       Outdated now: use numpy.percentile'''
    if len(v) < 3:
        return np.nan
    else:
        v2 = np.sort(v)
        idx = np.floor(q*len(v))
        return v2[int(idx)]
#-----------------------------------------------------------------------------#
def sample_data_from_TH2(file_path='', histo_name='', flag_S1cut=False):
    '''Read in a ROOT.TH2 object with an S1-S2 map. Return random S1-S2 pairs
    distributed according to the cell-contents of the 2D histogram
    '''
    f = ROOT.TFile.Open(file_path,'read')
    hh = f.Get(histo_name)
    if not hh:
        raise RuntimeError('Unable to retrieve histogram {} from {}'.format(histo_name,file_path))
    nSamples = hh.GetEntries() #events that passed NEST cuts
    nSamples = int(nSamples)
    X = np.zeros(nSamples, dtype=float) #create float array of size nSamples
    Y = np.zeros(nSamples, dtype=float)
    xx = np.zeros(1, dtype=float) #create float arrays of size 1 (to use TH2::GetRandom2)
    yy = np.zeros(1, dtype=float)
    for i in range(nSamples):
        hh.GetRandom2(xx,yy)
        X[i] = xx
        Y[i] = yy

    f.Close()
    return X,Y
#-----------------------------------------------------------------------------#
def read_data_from_RooDataSet(file_path, object_name):
    ''' 
    Read data from a ROOT.RooDataSet object
    '''
    f = ROOT.TFile(file_path)
    dataset = f.Get(object_name)
    nSamples = int(dataset.sumEntries())
    X = np.zeros(nSamples, dtype=float) 
    Y = np.zeros(nSamples, dtype=float)
    for i in range(nSamples):
        row = dataset.get(i)
        X[i] = row.getRealValue('S1')
        Y[i] = row.getRealValue('logS2')

    f.Close()
    return np.array(X), np.array(Y)
#-----------------------------------------------------------------------------#
def read_data_from_TGraph(file_path, object_name):
    ''' 
    Read data from a ROOT.TGraph object
    '''
    f = ROOT.TFile(file_path)
    data = f.Get(object_name)
    nSamples = int(data.GetN())
    X = np.zeros(nSamples, dtype=float) 
    Y = np.zeros(nSamples, dtype=float)
    xx = np.zeros(1, dtype=float) #create float arrays of size 1
    yy = np.zeros(1, dtype=float)
    for i in range(nSamples):
        data.GetPoint(i,xx,yy)
        X[i] = xx
        Y[i] = yy

    f.Close()
    return X,Y
#-----------------------------------------------------------------------------#
def read_data_from_TTree(file_path, object_name):
    ''' 
    Read data from a ROOT.TTree object
    '''
    f = ROOT.TFile(file_path)
    tree = f.Get(object_name)
    xx = 0.
    yy = 0.
    X = []
    Y = []
    for ev in tree:
        xx = ev.S1c
        yy = ev.S2c
        if(xx>0. and yy>0.):
            # only save those events with yy non-negative, to avoid problem with log(0) later on
            X.append(xx)
            Y.append(yy)

    f.Close()
    return np.array(X),np.array(Y)
#-----------------------------------------------------------------------------#
def apply_style_1D(title='',xlabel='Nuclear recoil energy [keV]',ylabel='Event rate [/kg/day/keV]', \
                   x_min=-1,x_max=-1,xtick_minor=2,xtick_major=10,text_size=18, grid=False):
    '''Common style settings for plots with 1D data'''

    # Style 
    plt.rcParams['text.usetex'] = True
    plt.rcParams['font.family'] = 'serif'
    plt.rcParams['font.sans-serif'] = 'Times'

    # Axis options
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xlim([x_min,x_max])

    # Plot options
    if(grid):
        plt.grid(True,zorder=0)

    # Set frequency ticks (X-axis)
    # plt.gca().xaxis.set_major_formatter(FormatStrFormatter('%g')) #0.1, 1, 10 ...
    plt.gca().xaxis.set_minor_locator(MultipleLocator(xtick_minor))
    plt.gca().xaxis.set_major_locator(MultipleLocator(xtick_major))
    # Set lenght of ticks
    plt.gca().tick_params(which='major', direction='in', top=1, right=1, length=8, pad=8)
    plt.gca().tick_params(which='minor', direction='in', top=1, right=1, length=4, pad=8)

    # this has to be at the end to apply to all elements of the plot
    plt.rcParams.update({'font.size': text_size})

    return
#-----------------------------------------------------------------------------#
def apply_style_2D(title='',xlabel='$\mathrm{S1_c}$ [phd]',ylabel='$\log_{10}$($\mathrm{S2_c}$ [phd])', \
                        x_min=0.,x_max=100,y_min=2.5,y_max=5.5,text_size=18, grid=False, \
                        xtick_minor=2,xtick_major=10,ytick_minor=0.1,ytick_major=0.5):
    '''Common style settings for plots with 2D data'''

    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xlim([x_min,x_max])
    plt.ylim([y_min,y_max])
    plt.rcParams['text.usetex'] = True
    plt.rcParams['font.family'] = 'serif'
    plt.rcParams['font.sans-serif'] = 'Times'

    if(grid):
        plt.grid(True,zorder=0)

    plt.gca().xaxis.set_minor_locator(MultipleLocator(xtick_minor))
    plt.gca().xaxis.set_major_locator(MultipleLocator(xtick_major))
    plt.gca().yaxis.set_minor_locator(MultipleLocator(ytick_minor))
    plt.gca().yaxis.set_major_locator(MultipleLocator(ytick_major))
    plt.gca().tick_params(which='major', direction='in', top=1, right=1, length=8, pad=8)
    plt.gca().tick_params(which='minor', direction='in', top=1, right=1, length=4, pad=8)

    # keep at the end
    plt.rcParams.update({'font.size': text_size})

    return
#-----------------------------------------------------------------------------#
